//
//  ContentView.swift
//  audio-engine
//
//  Created by DJ Flux on 3/29/20.
//  Copyright © 2020 Flux Labs. All rights reserved.
//

import SwiftUI

struct ContentView: View {
    var body: some View {
        Text("Hello, World!")
    }
}

struct ContentView_Previews: PreviewProvider {
    static var previews: some View {
        ContentView()
    }
}
